plugin.tx_hiveovrrealurl {
    settings {

    }
}


config {
    simulateStaticDocuments = 0

    tx_realurl_enable = 1
    prefixLocalAnchors = all
    absRelPath = /
    # absRefPrefix =
    # typolinkEnableLinksAcrossDomains = 1
}