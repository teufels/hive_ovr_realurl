<?php
namespace HIVE\HiveOvrRealurl\Hooks;
/**
 * This file is part of the "hive_ovr_realurl" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * AutoConfiguration-Hook for RealURL
 *
 */
class RealUrlAutoConfiguration
{
    /**
     * Generates additional RealURL configuration and merges it with provided configuration
     *
     * @param       array $params Default configuration
     * @return      array Updated configuration
     */
    public function addConfig($params)
    {
        return array_merge_recursive(
            $params['config'],
            [
                'init' => [
                    'enableCHashCache' => 1,
                    //'appendMissingSlash' => 'ifNotFile,redirect',
                    //'emptyUrlReturnValue' => '/',
                    'enableUrlDecodeCache' => 1,
                    'enableUrlEncodeCache' => 1,
                    'respectSimulateStaticURLs'	=>	0,
                    'postVarSet_failureMode' => '',
                ],
                'pagePath' => [
                    'spaceCharacter' => '-',
                    'languageGetVar' => 'L',
                    'expireDays' => 3,
                    'firstHitPathCache' => 1,
                ],
                'redirects' => [],
                // 'preVars' => [
                //     [
                //         'GETvar' => 'no_cache',
                //         'valueMap' => [
                //             'nc' => 0,
                //             'no_cache' => 0,
                //         ],
                //         'noMatch' => 'bypass',
                //     ],
                // ],
                'lastGenerated' => time()
            ]
        );
    }

    /**
     * @param array $configuration
     */
    public function postProcessConfiguration(array &$parameters) {
//        $parameters['config']['_DOMAINS'] = [];
    }
}