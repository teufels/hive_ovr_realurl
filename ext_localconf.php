<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
// Use RealURL AutoConf feature
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('realurl')) {
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['realurl'] = serialize(
        [
            'configFile' => '',
            'enableAutoConf' => 1,
            'autoConfFormat' => 0 // serialized
        ]
    );
    if (strpos($_SERVER["HTTP_HOST"] , "development." ) !== false) {
        $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['realurl'] = serialize(
            [
                'configFile' => '', // typo3conf/ext/hive_thm_custom/realurl_conf.php'
                'enableAutoConf' => 1,
                'autoConfFormat' => 1 // not serialized...
            ]
        );
    }

    /*
     * Hooks
     */
    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('realurl')) {
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['extensionConfiguration']['hive_ovr_realurl'] =
            \HIVE\HiveOvrRealurl\Hooks\RealUrlAutoConfiguration::class . '->addConfig';

        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['postProcessConfiguration']['hive_ovr_realurl'] =
            \HIVE\HiveOvrRealurl\Hooks\RealUrlAutoConfiguration::class . '->postProcessConfiguration';
    }
}